from django.db import models

# Create your models here.

class Library(models.Model):
    name = models.CharField(max_length=200,default="")
    phone = models.CharField(max_length=200,default="")
    computers = models.CharField(max_length=200,default="")
    wifi = models.CharField(max_length=10,default="")
    district = models.CharField(max_length=200,default="")