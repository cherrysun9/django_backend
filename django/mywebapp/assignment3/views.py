from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from http import HTTPStatus
from .models import Library
import re
import json

def index(request):
    return HttpResponse("Hello, world. You're at assignment3 index.")

@csrf_exempt
def handle_libraries(request):
    if request.method == 'GET':
        libraries = {} # Return all the libraries
        return JsonResponse(libraries, status=HTTPStatus.OK)
        
    if request.method == 'POST':
        data = request.body.decode('utf-8')
        data_json = json.loads(data)
        
        if "name" in data_json:
            if data_json['name'] == "":
                return JsonResponse({"message": "name cannot be empty."}, status=400)
            else:
                library_name = data_json['name']
        else:
            return JsonResponse({"message": "name cannot be empty."}, status=400)
            
        if "phone" in data_json:
            if data_json['phone'] == "":
                return JsonResponse({"message": "phone cannot be empty."}, status=400)
            else:
                phone_number = data_json['phone']
        else:
            return JsonResponse({"message": "phone cannot be empty."}, status=400)
        
        if "computers" in data_json:
            if data_json['computers'] == "":
                return JsonResponse({"message": "computers cannot be empty."}, status=400)
            else:
                computers = data_json['computers']
        else:
            return JsonResponse({"message": "computers cannot be empty."}, status=400)
      
        if "wifi" in data_json:
            if data_json['wifi'] == "":
                return JsonResponse({"message": "wifi cannot be empty."}, status=400)
            else:
                wifi = data_json['wifi']
        else:
            return JsonResponse({"message": "wifi cannot be empty."}, status=400)
            
        if "district" in data_json:
            if data_json['district'] == "":
                return JsonResponse({"message": "district cannot be empty."}, status=400)
            else:
                district = data_json['district']
        else:
            return JsonResponse({"message": "district cannot be empty."}, status=400)
      
        library_name = data_json['name']
        phone_number = data_json['phone']
        computers = data_json['computers']
        wifi = data_json['wifi']
        district = data_json['district']
        print("Received Library with name:" + library_name)
    
        if len(phone_number)!= 12:
            return JsonResponse({"message": "phone not in valid format."}, status=400)

        for i in range(0,len(phone_number)):
            cur = phone_number[i]
            if i == 3 or i == 7:
                if phone_number[i]!='-':
                    return JsonResponse({"message": "phone not in valid format."}, status=400)
            else: 
                if cur=='0' or cur=='1' or cur=='2' or cur=='3' or cur=='4' or cur=='5' or cur=='6' or cur=='7' or cur=='8' or cur=='9':
                    pass
                else:
                    return JsonResponse({"message": "phone not in valid format."}, status=400)
        
        for k, v in data_json.items():
            if k == 'name' or k == 'phone' or k=='computers' or k=='wifi' or k=='district':
                pass
            else:    
                return JsonResponse({"message": str(k)+" not recognized."}, status=400) 
        
        library = Library(name=library_name, phone = phone_number, computers = computers, wifi = wifi, district = district)
        library.save()
        library_id = library.id
        library_response = {}
        library_response["name"] = library_name
        library_response["phone"] = phone_number
        library_response["computers"] = computers
        library_response["wifi"] = wifi
        library_response["district"] = district
        r = HttpResponse()
        r["location"] = "http://127.0.0.1:8000/assignment3/libraries/" + str(library_id)
        r.status_code = 201
        return r


@csrf_exempt
def handle_single_library(request, library_id):
    library = {}
    if request.method == 'GET':
        libraryObj = Library.objects.filter(id=library_id).first()
        if libraryObj!=None:
            library['name'] = libraryObj.name
            library['phone'] = libraryObj.phone
            library['computers'] = libraryObj.computers
            library['wifi'] = libraryObj.wifi
            library['district'] = libraryObj.district
            return JsonResponse(library, status=HTTPStatus.OK)
        else:
            return JsonResponse({"message": "library with id " + str(library_id) + " not found."}, status=404)
            
    if request.method == 'PUT':
        libraryObj = Library.objects.filter(id=library_id).first()
        if libraryObj == None:
            return JsonResponse({"message": "library with id "+str(library_id)+" not found."}, status=404)
        data = request.body.decode('utf-8')
        data_json = json.loads(data)
        
        if "name" in data_json:
            if data_json['name'] == "":
                return JsonResponse({"message": "name cannot be empty."}, status=400)
            else:
                library_name = data_json['name']
        else:
            return JsonResponse({"message": "name cannot be empty."}, status=400)
            
        if "phone" in data_json:
            if data_json['phone'] == "":
                return JsonResponse({"message": "phone cannot be empty."}, status=400)
            else:
                phone_number = data_json['phone']
        else:
            return JsonResponse({"message": "phone cannot be empty."}, status=400)
        
        if "computers" in data_json:
            if data_json['computers'] == "":
                return JsonResponse({"message": "computers cannot be empty."}, status=400)
            else:
                computers = data_json['computers']
        else:
            return JsonResponse({"message": "computers cannot be empty."}, status=400)
      
        if "wifi" in data_json:
            if data_json['wifi'] == "":
                return JsonResponse({"message": "wifi cannot be empty."}, status=400)
            else:
                wifi = data_json['wifi']
        else:
            return JsonResponse({"message": "wifi cannot be empty."}, status=400)
            
        if "district" in data_json:
            if data_json['district'] == "":
                return JsonResponse({"message": "district cannot be empty."}, status=400)
            else:
                district = data_json['district']
        else:
            return JsonResponse({"message": "district cannot be empty."}, status=400)
            
        library_name = data_json['name']
        phone_number = data_json['phone']
        computers = data_json['computers']
        wifi = data_json['wifi']
        district = data_json['district']
        
        if len(phone_number)!= 12:
            return JsonResponse({"message": "phone not in valid format."}, status=400)

        for i in range(0,len(phone_number)):
            cur = phone_number[i]
            if i == 3 or i == 7:
                if phone_number[i]!='-':
                    return JsonResponse({"message": "phone not in valid format."}, status=400)
            else: 
                if cur=='0' or cur=='1' or cur=='2' or cur=='3' or cur=='4' or cur=='5' or cur=='6' or cur=='7' or cur=='8' or cur=='9':
                    pass
                else:
                    return JsonResponse({"message": "phone not in valid format."}, status=400)
        
        for k, v in data_json.items():
            if k == 'name' or k == 'phone' or k=='computers' or k=='wifi' or k=='district':
                pass
            else:    
                return JsonResponse({"message": str(k)+" not recognized."}, status=400) 
            
        libraryObj.name = library_name
        libraryObj.phone = phone_number
        libraryObj.computers = computers
        libraryObj.wifi = wifi
        libraryObj.district = district 
        libraryObj.save()
        
        library_response = {}
        library_response["name"] = library_name
        library_response["phone"] = phone_number
        library_response["computers"] = computers
        library_response["wifi"] = wifi
        library_response["district"] = district  
        return JsonResponse(library_response, status=HTTPStatus.OK)
        
    if request.method == 'PATCH':
        libraryObj = Library.objects.filter(id = library_id).first()
        if libraryObj == None:
            return JsonResponse({"message": "library with id "+str(library_id)+" not found."}, status=404)
        else:
            data = request.body.decode('utf-8')
            data_json = json.loads(data)
            if data_json == {}:
                return JsonResponse({"message": "request body cannot be empty."},status=400)
            
            if "name" in data_json:
                if data_json['name'] == "":
                    return JsonResponse({"message": "name cannot be empty."}, status=400)
                else:
                    libraryObj.name = data_json['name']
                     
            if "phone" in data_json:  
                if data_json["phone"] == "":
                    return JsonResponse({"message": "phone cannot be empty."}, status=400)
                    
                phone_number = data_json['phone']
                if len(phone_number)!= 12:
                    return JsonResponse({"message": "phone not in valid format."}, status=400)
                for i in range(0,len(phone_number)):
                    cur = phone_number[i]
                    if i == 3 or i == 7:
                        if phone_number[i]!='-':
                            return JsonResponse({"message": "phone not in valid format."}, status=400)
                    else: 
                        if cur=='0' or cur=='1' or cur=='2' or cur=='3' or cur=='4' or cur=='5' or cur=='6' or cur=='7' or cur=='8' or cur=='9':
                            pass
                        else:
                            return JsonResponse({"message": "phone not in valid format."}, status=400)
                libraryObj.phone = phone_number
            
            if "computers" in data_json:
                if data_json['computers'] == "":
                    return JsonResponse({"computers": "computers cannot be empty."}, status=400)
                else:
                    libraryObj.computers = data_json['computers']
                    
            if "wifi" in data_json:
                if data_json['wifi'] == "":
                    return JsonResponse({"message": "wifi cannot be empty."}, status=400)
                else:
                    libraryObj.wifi = data_json['wifi']
            if "district" in data_json:
                if data_json["district"] == "":
                    return JsonResponse({"message": "district cannot be empty."}, status=400)
                else:
                    libraryObj.district = data_json['district']
            for k, v in data_json.items():
                if k == 'name' or k == 'phone' or k=='computers' or k=='wifi' or k=='district':
                    pass
                else:    
                    return JsonResponse({"message": str(k)+" not recognized."}, status=400) 
                    
            library_response = {}
            library_response["name"] = libraryObj.name
            library_response["phone"] = libraryObj.phone
            library_response["computers"] = libraryObj.computers
            library_response["wifi"] = libraryObj.wifi
            library_response["district"] = libraryObj.district
            return JsonResponse(library_response, status=HTTPStatus.OK)
            
            
    if request.method == 'DELETE':
        libraryObj = Library.objects.filter(id=library_id).first()
        if libraryObj == None:
            return JsonResponse({"message": "library with id "+str(library_id)+" not found."}, status=404)
        else:
            Library.objects.filter(id=library_id).delete()
            return HttpResponse(status=200)
            
